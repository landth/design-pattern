<?php

class CarAPI {
	public function __construct() {}
	
	public function getCar($car_id) {
		echo "You want to see detail of car with ID: " . $car_id . "<br />";
	}
}

interface detailAdapter {
	public function getDetail($car_id);
}

class CarAdapter implements detailAdapter {
	private $car;
	
	public function __construct($_car) {
		$this->car = $_car;
	}
	
	public function getDetail($car_id) {
		$this->car->getCar($car_id);
	}
}

echo "--- Get detail with class CarAPI <br />";
$carApi = new CarAPI();
$carApi->getCar(10001);

echo "--- Get detail with class CarAdapter <br />";
$carAdapter = new CarAdapter(new CarAPI());
$carAdapter->getDetail(10001);

