<?php
class A {
	public function __construct() {}
	
	public function processA() {
		echo 'Process A task <br />';
	}
}

class B {
	public function __construct() {}
	
	public function processB() {
		echo 'Process B task <br />';
	}
}

class C {
	public function __construct() {}
	
	public function processC() {
		echo 'Process C task <br />';
	}
}

class ProcessFacde {
	public $a = null;
	public $b = null;
	public $c = null;
	public function __construct() {
		$this->a = new A();
		$this->b = new B();
		$this->c = new C();
	}
	
	public function processTask() {
		$this->a->processA();
		$this->b->processB();
		$this->c->processC();
	}
}

$f = new ProcessFacde();
$f->processTask();










