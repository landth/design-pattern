<?php
class ClassicSingleton {
    private static $_instance;
	public $name = 'lando';
	
    public function __construct() {}
	
	public static function getInstance() {
		if (!self::$_instance) {
			self::$_instance = new ClassicSingleton();
		}
		return self::$_instance;
	}
}

echo 'Create object';
$o = ClassicSingleton::getInstance();
echo '<br />---------- The name before: ' . $o->name;
echo '<br />Change name';
$o->name = 'quoc khanh';
echo '<br />Destroy object';
unset($o);
echo '<br />Create new object';
$o = ClassicSingleton::getInstance();
echo '<br />---------- The name after: ' . $o->name;
