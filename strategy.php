<?php
abstract class Vehicle {
	
	private $goAlgorithm;
	
	public function setGoAlgorithm($__goAlgorithm) {
		$this->goAlgorithm = $__goAlgorithm;
	}
	
    public function go() {
        $this->goAlgorithm->go();
    }
}

interface goAlgorithm {
	public function go();
}

class GoByDrivingAlgorithm implements goAlgorithm {
	public function go() {
		echo "Now I'm driving <br />";
	}
}

class GoByFlyingAlgorithm implements goAlgorithm {
	public function go() {
		echo "Now I'm flying <br />";
	}
}

class GoByFlyingFastAlgorithm implements goAlgorithm {
	public function go() {
		echo "Now I'm flying very fast <br />";
	}
}

class StreetRacer extends Vehicle {}

class Helicopter extends Vehicle {}

class HelicopterFast extends Helicopter {}

$h = new StreetRacer();
$h->setGoAlgorithm(new GoByDrivingAlgorithm());
$h->go();

$h = new Helicopter();
$h->setGoAlgorithm(new GoByFlyingAlgorithm());
$h->go();

$hf = new HelicopterFast();
$hf->setGoAlgorithm(new GoByFlyingFastAlgorithm());
$hf->go();






